#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "list.h"
#include "mem.h"

/* newitem: create new item from name and value */
Nameval *newitem(char *name, int value) {
    Nameval *newp;

    newp = emalloc(sizeof(Nameval));
    newp->name = name;
    newp->value = value;
    newp->next = NULL;
    return newp;
}

/* addfront: add newp to front of listp */
Nameval *addfront(Nameval *listp, Nameval *newp) {
    newp->next = listp;
    return newp;
}

/* addend: add newp to end of listp */
Nameval *addend(Nameval *listp, Nameval *newp) {
    Nameval *p;
    
    if (listp == NULL)
        return newp;
    for (p = listp; p->next != NULL; p = p->next)
        ;
    p->next = newp;
    return listp;
}

/* lookup: sequential search for name in listp*/
Nameval *lookup(Nameval *listp, char *name) {
    for ( ; listp != NULL; listp = listp->next)
        if (strcmp(name, listp->name) == 0)
            return listp;
    return NULL; /* no match */
}

/* apply: execute fn for each element of listp */
void apply(Nameval *listp,
        void (*fn)(Nameval*, void*), void *arg) {
    for ( ; listp != NULL; listp = listp->next) {
        (*fn)(listp, arg); /* call the function */
    }
}

/* printnv: print name and value using format in arg */
void printnv(Nameval *p, void *arg) {
    char *fmt;

    fmt = (char *)arg;
    printf(fmt, p->name, p->value);
}

/* inccounter: increment integer counter *arg */
void inccounter(Nameval *p, void *arg) {
    int *ip;

    /* p is unused */
    ip = (int *)arg;
    (*ip)++;
}

/* freeall: free all elements of listp */
void freeall(Nameval *listp) {
    Nameval *next;

    for ( ; listp != NULL; listp = next) {
        next = listp->next;
        /* Assumes name is freed elsewhere */
        free(listp);
    }
}

/* delitem: delete first "name" from listp */
Nameval *delitem(Nameval *listp, char *name) {
    Nameval *p;
    Nameval **head = &listp;
    Nameval **curr;

    for (curr = head; *curr; ) {
        p = *curr;
        if (strcmp(name, p->name) == 0) {
            *curr = p->next;
            free(p);
            return listp;
        } else {
            curr = &p->next;
        }
    }

    printf("delitem: %s not in list", name);
    return NULL;
}

/* appends listrhs to the end of listp */
Nameval *append_list(Nameval *listp, Nameval *listrhs) {
    for ( ; listrhs != NULL; listrhs = listrhs->next) {
        listp = addend(listp, newitem(listrhs->name, listrhs->value));
    }
    return listp;
}
 
/* copy: create a copy of a list */
Nameval *copy(Nameval *listp) {
    Nameval *newp = NULL;
    newp = append_list(newp, listp);
    return newp;
}

/* merge: merges two lists into one */
Nameval *merge(Nameval *listp1, Nameval *listp2) {
    Nameval *newp = NULL;

    newp = append_list(newp, listp1);
    newp = append_list(newp, listp2);

    return newp;
}
