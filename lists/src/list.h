#ifndef LIST_H
#define LIST_H

typedef struct Nameval Nameval;
struct Nameval {
    char *name;
    int value;
    Nameval *next; /* in list */
};

/* newitem: create new item from name and value */
Nameval *newitem(char *name, int value);

/* addfront: add newp to front of listp */
Nameval *addfront(Nameval *listp, Nameval *newp);

/* addend: add newp to end of listp */
Nameval *addend(Nameval *listp, Nameval *newp);

/* lookup: sequential search for name in listp*/
Nameval *lookup(Nameval *listp, char *name);

/* apply: execute fn for each element of listp */
void apply(Nameval *listp, void (*fn)(Nameval*, void*), void *arg);

/* printnv: print name and value using format in arg */
void printnv(Nameval *p, void *arg);

/* inccounter: increment integer counter *arg */
void inccounter(Nameval *p, void *arg);

/* freeall: free all elements of listp */
void freeall(Nameval *listp);

/* delitem: delete first "name" from listp */
Nameval *delitem(Nameval *listp, char *name);

/* copy: create a copy of a list */
Nameval *copy(Nameval *listp);

/* merge: merges two lists into one */
Nameval *merge(Nameval *listp1, Nameval *listp2);

#endif /* LIST_H */
