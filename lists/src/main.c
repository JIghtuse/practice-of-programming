#include <stdio.h>
#include "list.h"

static void print_list(Nameval *listp) {
    if (listp == NULL)
        return;
    apply(listp, printnv, "%s: 0x%x\n");
}

int get_list_sz(Nameval *listp) {
    int n;

    n = 0;
    apply(listp, inccounter, &n);
    return n;
}

int main(void) {
    Nameval *p;
    Nameval *nvlist = NULL;
    Nameval *nvcopy = NULL;
    Nameval *nvmerge = NULL;

    printf("List contents: \n");
    print_list(nvlist);

    nvlist = addfront(nvlist, newitem("smiley", 0x263A));
    printf("List contents after addfront: \n");
    print_list(nvlist);

    nvlist = addend(nvlist, newitem("zeta", 0x03b6));
    printf("List contents after addend: \n");
    print_list(nvlist);

    nvcopy = copy(nvlist);
    nvmerge = merge(nvlist, nvcopy);

    p = lookup(nvlist, "zeta");
    if (p != NULL) {
        printf("Found 'zeta': %p\n", (void *)p);
    }

    printf("List size: %d\n", get_list_sz(nvlist));
    nvlist = delitem(nvlist, "smiley");
    printf("List contents after delitem: \n");
    print_list(nvlist);

    freeall(nvlist);

    printf("Copy list contents:\n");
    print_list(nvcopy);

    freeall(nvcopy);

    printf("Merged list contents:\n");
    print_list(nvmerge);

    freeall(nvmerge);

    return 0;
}
