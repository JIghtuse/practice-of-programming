#ifndef HASHTABLE_H
#define HASHTABLE_H

typedef struct Nameval Nameval;
struct Nameval {
    char *name;
    int value;
    Nameval *next; /* in chain */
};

enum {
    NHASH = 128, /* symbol table size */
    MULTIPLIER = 31 /* empirically selected reasonable choice for ASCII strings */
};

Nameval *symtab[NHASH]; /* a symbol table */

/* lookup: find name in symtab, with optional create */
Nameval *lookup(char *name, int create, int value);

/* hash: compute hash value of string */
unsigned int hash(char *str);

/* print_table: print contents of symbol table */
void print_table(void);

#endif /* HASHTABLE_H */
