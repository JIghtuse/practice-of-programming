#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "hashtable.h"
#include "mem.h"

/* lookup: find name in symtab, with optional create */
Nameval *lookup(char *name, int create, int value) {
    int h;
    Nameval *sym;

    h = hash(name);
    for (sym = symtab[h]; sym != NULL; sym = sym->next) {
        if (strcmp(name, sym->name) == 0) {
            return sym;
        }
    }
    if (create != 0) {
        sym = emalloc(sizeof *sym);
        sym->name = name; /* assumed allocation elsewhere */
        sym->value = value;
        sym->next = symtab[h];
        symtab[h] = sym;
    }
    return sym;
}

/* hash: compute hash value of string */
unsigned int hash(char *str) {
    unsigned int h;
    unsigned char *p;

    h = 0;
    for (p = (unsigned char *)str; *p != '\0'; p++)
        h = MULTIPLIER * h + *p;
    return h % NHASH;
}

/* print_table: print contents of symbol table */
void print_table(void) {
    unsigned int i;
    Nameval *p;

    for (i = 0; i < NHASH; i++) {
        for (p = symtab[i]; p != NULL; p = p->next) {
            printf("%s: 0x%x (hash: %d)\n", p->name, p->value, i);
        }
    }
}
