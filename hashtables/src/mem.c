#include "mem.h"
#include <stdio.h>

void *emalloc(size_t sz) {
    void *p = malloc(sz);
    if (p == NULL) {
        fprintf(stderr, "Cannot allocate %u bytes of memory", (unsigned)sz);
        exit(EXIT_FAILURE);
    }
    return p;
}
