#include <stdio.h>
#include "hashtable.h"

int main(void) {
    Nameval *nvhashtable = NULL;
    
    nvhashtable = lookup("smiley", 1, 0x263a);
    nvhashtable = lookup("Aacute", 1, 0x00c1);
    nvhashtable = lookup("zeta", 1, 0x03b6);
    nvhashtable = lookup("AElig", 1, 0x00c6);
    nvhashtable = lookup("Acirc", 1, 0x00c2);

    print_table();

    return 0;
}
