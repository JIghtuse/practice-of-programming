#include <stdio.h>
#include "quicksort.h"

#define NELEM(a) (sizeof(a) / sizeof(a[0]))

int main(void) {
    size_t i;
    int a[] = { 6, -2, -41, 4, 0, 15, 3, -13, 8 };

    printf("Original array: ");
    for (i = 0; i < NELEM(a); i++) {
        printf("%d ", a[i]);
    }
    printf("\nSorted array: ");
    quicksort(a, NELEM(a));
    for (i = 0; i < NELEM(a); i++) {
        printf("%d ", a[i]);
    }
    printf("\n");

    return 0;
}
