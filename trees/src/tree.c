#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "tree.h"
#include "mem.h"

/* newitem: creates new Nameval from name and value */
Nameval *newitem(char *name, int value) {
    Nameval *p;

    p = emalloc(sizeof(*p));
    p->name = name;
    p->value = value;
    p->left = NULL;
    p->right = NULL;
    return p;
}

/* insert: insert newp in treep, return treep */
Nameval *insert(Nameval *treep, Nameval *newp) {
    int cmp;

    if (treep == NULL)
        return newp;
    cmp = strcmp(newp->name, treep->name);
    if (cmp == 0)
        fprintf(stderr, "insert: duplicate entry %s ignored\n", newp->name);
    else if (cmp < 0)
        treep->left = insert(treep->left, newp);
    else
        treep->right = insert(treep->right, newp);
    return treep;
}

/* lookup: look up name in tree treep */
Nameval *lookup(Nameval *treep, char *name) {
    int cmp;

    if (treep == NULL)
        return NULL;

    cmp = strcmp(name, treep->name);
    if (cmp == 0)
        return treep;
    else if (cmp < 0)
        return lookup(treep->left, name);
    else
        return lookup(treep->right, name);
}

/* nrlookup: non-recursively look up name in tree treep */
Nameval *nrlookup(Nameval *treep, char *name) {
    int cmp;

    while (treep != NULL) {
        cmp = strcmp(name, treep->name);
        if (cmp == 0)
            return treep;
        else if (cmp < 0)
            treep = treep->left;
        else
            treep = treep->right;
    }
    return NULL;
}

/* applyinorder: inorder application of fn to treep */
void applyinorder(Nameval *treep,
        void (*fn)(Nameval *, void*), void *arg) {
    
    if (treep == NULL)
        return;

    applyinorder(treep->left, fn, arg);
    (*fn)(treep, arg);
    applyinorder(treep->right, fn, arg);
}

/* printnv: print name and value using format in arg */
void printnv(Nameval *p, void *arg) {
    char *fmt;
    
    fmt = (char *)arg;
    printf(fmt, p->name, p->value);
}

/* applypostorder: postorder application of fn to treep */
void applypostorder(Nameval *treep,
        void (*fn)(Nameval *, void*), void *arg) {
    
    if (treep == NULL)
        return;

    applyinorder(treep->left, fn, arg);
    applyinorder(treep->right, fn, arg);
    (*fn)(treep, arg);
}
