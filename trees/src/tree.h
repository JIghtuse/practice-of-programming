#ifndef TREE_H
#define TREE_H

typedef struct Nameval Nameval;
struct Nameval {
    char *name;
    int value;
    Nameval *left;  /* lesser */
    Nameval *right; /* greater */
};

/* newitem: creates new Nameval from name and value */
Nameval *newitem(char *name, int value);

/* insert: insert newp in treep, return treep */
Nameval *insert(Nameval *treep, Nameval *newp);

/* lookup: look up name in tree treep */
Nameval *lookup(Nameval *treep, char *name);

/* nrlookup: non-recursively look up name in tree treep */
Nameval *nrlookup(Nameval *treep, char *name);

/* applyinorder: inorder application of fn to treep */
void applyinorder(Nameval *treep, void (*fn)(Nameval *, void*), void *arg);

/* applypostorder: postorder application of fn to treep */
void applypostorder(Nameval *treep, void (*fn)(Nameval *, void*), void *arg);

/* printnv: print name and value using format in arg */
void printnv(Nameval *p, void *arg);

#endif /* TREE_H */
