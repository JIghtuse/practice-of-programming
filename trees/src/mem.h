#ifndef MEM_H
#define MEM_H

#include <stdlib.h>
void *emalloc(size_t sz);

#endif /* MEM_H */
