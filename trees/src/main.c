#include <stdio.h>
#include "tree.h"

int main(void) {
    Nameval *nvtree = NULL;

    nvtree = insert(nvtree, newitem("smiley", 0x263a));
    nvtree = insert(nvtree, newitem("Aacute", 0x00c1));
    nvtree = insert(nvtree, newitem("zeta", 0x03b6));
    nvtree = insert(nvtree, newitem("AElig", 0x00c6));
    nvtree = insert(nvtree, newitem("Acirc", 0x00c2));

    applyinorder(nvtree, printnv, "%s: 0x%x\n");

    return 0;
}
