#ifndef ARRAY_H
#define ARRAY_H

typedef struct Nameval Nameval;
struct Nameval {
    int value;
    char *name;
};

struct NVtab {
    int nval; /* current number of values */
    int max;  /* allocated number of values */
    Nameval *nameval; /* array of name-value pairs */
} nvtab;

int addname(Nameval newname);
int delname(char *name);

#endif /* ARRAY_H */
