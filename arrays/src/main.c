#include <stdio.h>
#include "array.h"

static void print_array() {
    int i;

    printf("Array (sz: %d, max: %d) contents:\n", nvtab.nval, nvtab.max);
    for (i = 0; i < nvtab.nval; i++) {
        printf("[%d] %s\n", nvtab.nameval[i].value, nvtab.nameval[i].name);
    }
}

int main(void) {
    Nameval n = {
        /*.value = 7,
        .name = "Sedmoj"*/
        7,
        "Sedmoj"
    };
    print_array();

    addname(n);
    print_array();

    n.value = 8;
    n.name = "Vosmoj";
    addname(n);
    n.value = 1;
    n.name = "Pervyj";
    addname(n);
    print_array();

    delname("Sedmoj");
    print_array();

    return 0;
}
